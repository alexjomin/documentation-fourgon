pdf:
	asciidoctor-pdf --theme basic -a pdf-themesdir=theme -a pdf-fontsdir=theme/fonts documentation.adoc

publish: pdf
	git add .
	git commit -m "update documentation"
	git push